#ifndef SINGLEEPOCH_H
#define SINGLEEPOCH_H

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#define PI                      3.14159265358979
#define RAD2DEG 180/PI

#define MAX_CHANNELS        12
#define SPEED_OF_LIGHT      299792458.0
#define CODE_FREQ           1.023e6
#define CARRIER_FREQ        1.57542e9
#define L1_WAVELENGTH       (1/CARRIER_FREQ)*SPEED_OF_LIGHT
#define OMEGAE_DOT	        7.2921151467e-005
#define NAV_GM				3.986005e14

#define R2D 57.295779513082320876798154814105           /* radians to degrees */



struct s_misc_info
{

    double pos_diff_mag;
    double time_diff_mag;
    double final_pos_diff_mag;
    double final_time_diff_mag;
    unsigned final_iterations;
    double tg_correction;
    double debug[100];
    double pos_correction_mag;

    double recv_pos[3];
    double clock_bias;
    double tg;
    double pdop;

};

struct s_meas
{
	unsigned int    prn;
    double          ms_range;
	double          doppler;

    // secondary values
    double          PR;  // as calculated using the code phase meas
    double          PR2;  // "truth" value

};

typedef struct s_meas s_meas;

struct s_nav_packet
{
	unsigned int    gps_week;
	double          gps_seconds;
	double          time_init;
	double          elapsed_time;
	double          pos_truth[3];
	double          pos_init[3];
	unsigned int    num_meas;

    s_meas          Meas[MAX_CHANNELS];
};

struct s_PVT_Info2
{

   unsigned int         prn;

   double               satposxyz[3];
   double               satvelxyz[3];
   double               doppler;

   double               satclk_bias;
   double               satclk_drift;

   double               TOA_m;
   double               pseudorange;
   double               pseudorange_dot;

};

typedef struct s_nav_packet s_nav_packet;
typedef struct s_PVT_Info2 s_PVT_Info2;
typedef struct s_misc_info s_misc_info;

#endif
