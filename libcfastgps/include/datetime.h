//    datetime.h
//
//     date and time routines for GPS Processing
//     This is a modfied version of the original routines created by
//     Charles Schwarz and Gordon Adams.  The functions were modified to
//     use the Remondi Date/Time algorithms (see Hilla & Jackson (2000),
//     The GPS Toolbox: The Remondi Date/Time Algorithms, GPS Solutions,
//     3(4), 71-74 ).
//
//  DateTime provides the following formats for representing a GPS-time:
//         1. GPS time (GPS week and second of week)
//         2. Modified Julian Date (MJD and fraction-of-a-day)
//         3. Year, day-of-year, hour, minute, seconds
//         4. Year, Month, day, hour, minute, seconds
//
// An object of the class DateTime may be initialized (constructed) from
//   any of the representations and viewed in any other representation.
//
// Translation in C and removing of useless features by Marco Dalla Mutta (2018)

#if !defined( DATETIME_ )
#define  DATETIME_

typedef struct {

        long GPSWeek;
        double secsOfWeek;

}GPSTime;

typedef struct {

    long mjd;
    double fractionOfDay;

}DateTime;

DateTime _DateTime();
DateTime GPSDateTime(GPSTime gpstime);

void  SetYMDHMS2(DateTime *datetime, long year, long month, long day,
	                  long hour, long min, double sec);

DateTime timesum(DateTime datetime, const double days);
double timediff(DateTime DT1, DateTime DT2);

#endif