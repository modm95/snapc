/*
* Copyright (c) 2008, Morgan Quigley, Pieter Abbeel and Scott Gleason
* All rights reserved.
*
* Originally written by Morgan Quigley and Pieter Abbeel
* Additional contributions by Scott Gleason
*
* This file is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 2 of the License, or
* (at your option) any later version.
*
* Fastgps is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with fastgps.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "fastgps.h"
#include "kiss_fft.h"
#include "kiss_fftr.h"

void fine_acquisition(struct channel *ch);

char acq_buf[153000];     // sampling freq 38.192e6 max

unsigned acq_buf_write_pos;

static kiss_fft_cpx *code_fft[MAX_SATELLITES];
static kiss_fft_cfg sample_fft_cfg, inverse_fft_cfg;
static int sample_idx;
static kiss_fft_cpx *sample_buf, *sample_fft, *mult_result, *inverse_fft;
static double *max_shift_energy;
static double  max_doppler, max_shift, max_energy;
extern gps_real_t dopplers[NUM_COARSE_DOPPLERS], 
                  fine_dopplers[NUM_FINE_DOPPLERS];
static double avg_energy;

FILE *acq_debug;
FILE *acq_debug2;

unsigned acquire2(unsigned cidx) {

    unsigned retval = FAILURE;

	// If this channel has already aquired, return
	if (c[cidx].state > CH_STATE_ACQUIRE)
    return(retval);

	if (acq_buf_write_pos >= system_vars.acq_buf_len)
		retval = acq_buffer_full2(cidx);

	return retval;

}

unsigned acq_buffer_full2(unsigned cidx)
{
  double tempd;
  unsigned s; // sample index (saves typing)
  unsigned retval = FAILURE;
  struct channel *ch = &c[cidx];
  unsigned acq_fft_len = kiss_fft_next_fast_size(system_vars.acq_buf_len);

  for (s = 0; s < system_vars.acq_buf_len; s++){
    max_shift_energy[s] = 0;
    sample_buf[s].r = 0;
    sample_buf[s].i = 0;
  }

  // Doppler loop
  for (ch->acq.doppler_idx = 0; ch->acq.doppler_idx < NUM_COARSE_DOPPLERS; 
       ch->acq.doppler_idx++)
  {
    int nco_sin, nco_cos, i, q;
    ch->car_phase = 0;
    ch->code_prompt = 0;
    avg_energy = 1;
    max_energy = 0;

    ch->car_phase_inc = 2 * M_PI * 
                        (system_vars.IF + dopplers[ch->acq.doppler_idx]) / 
                        system_vars.sampling_freq;

    // load I and Q samples into FFT buffer
  	for (s = 0; s < system_vars.acq_buf_len; s++)
    {
      char sample = acq_buf[s];
      nco_sin = GPS_SIN(ch->car_phase);
      nco_cos = GPS_COS(ch->car_phase);
      i =  sample * nco_cos;
      q = -sample * nco_sin;
      ch->car_phase += ch->car_phase_inc;
      UNWRAP_ANGLE(ch->car_phase);
      sample_buf[s].r = i;
      sample_buf[s].i = q;
    }

    // Perform FFT on data and perform multiply with (pre-calculated) PRN code FFT 
    kiss_fft(sample_fft_cfg, sample_buf, sample_fft);
    for (s = 0; s < system_vars.acq_buf_len; s++)
    {
      mult_result[s].r = sample_fft[s].r * code_fft[ch->prn_num-1][s].r -
                         sample_fft[s].i * code_fft[ch->prn_num-1][s].i;
      mult_result[s].i = sample_fft[s].r * code_fft[ch->prn_num-1][s].i + 
                         sample_fft[s].i * code_fft[ch->prn_num-1][s].r;
    }

    // Perform Inverse FFT of frequency domain multiplication
    kiss_fft(inverse_fft_cfg, mult_result, inverse_fft);
		
    // search the IFFT result for signal peaks
    //printf("%f\n", max_shift_energy[10]);
    for (s = 0; s < system_vars.acq_buf_len; s++)
    {
      double d = inverse_fft[s].r * inverse_fft[s].r + 
                 inverse_fft[s].i * inverse_fft[s].i;
      unsigned idx = s % (system_vars.acq_buf_len / ACQ_MS);
      if (d > max_shift_energy[idx])
      {
        max_shift_energy[idx] = d;
        if (d > max_energy)
        {
          max_doppler = dopplers[ch->acq.doppler_idx];
          ch->acq.best_doppler = ch->acq.doppler_idx;

          max_shift = (s % (acq_fft_len / ACQ_MS))   / 
                      (double)(acq_fft_len / ACQ_MS) * 
                      (double)(CHIPS_PER_CODE);

          max_energy = d;
        }
      }
      avg_energy += d;
    }    

    avg_energy /= (system_vars.acq_buf_len);
    tempd = max_energy / avg_energy;

    // If the max to avg ration indicates a signal is present
    if(tempd > COARSE_ACQ_THRESH)
    {
      fastgps_printf("coarse acq best doppler: %f\n", max_doppler);

      // Perform fine frequency scan, without FFT's
      fine_acquisition(ch);

      // Store results for tracking
      ch->true_car_phase_inc = 2 * M_PI * (system_vars.IF + ch->doppler) / 
                               system_vars.sampling_freq;
      ch->true_code_inc = (CODE_FREQ + ch->doppler * CARRIER_AID_SF) / 
                          system_vars.sampling_freq;
      ch->car_phase_inc = 2 * M_PI * (system_vars.IF + 0) / 
                          system_vars.sampling_freq;
      ch->code_inc = (CODE_FREQ + 0 * CARRIER_AID_SF) / 
                     system_vars.sampling_freq;
      ch->track.carrier_freq = system_vars.IF + ch->doppler;
      ch->track.carrier_freq_acq = ch->track.carrier_freq;
      ch->track.code_freq = CODE_FREQ + ch->doppler * CARRIER_AID_SF;
      ch->num_ms = 0;
      ch->tracking_lock = 0;
      ch->ie_accum = ch->qe_accum = 0;
      ch->ip_accum = ch->qp_accum = 0;
      ch->il_accum = ch->ql_accum = 0;
      ch->ie = ch->qe = ch->ip = ch->qp = ch->il = ch->ql = 0;
      ch->state = CH_STATE_POSTACQ_SPIN;
      ch->acq.max_energy = max_energy;
      ch->acq.ratio = tempd;
      retval = SUCCESS;
      fastgps_printf("freq: %f, doppler = %f, detection ratio: %f \n",
                     ch->track.carrier_freq, ch->doppler, tempd);
      break;
    }
  }   // end of Doppler loop


  // Debug processing
  if(system_vars.acq_log_flag == DEBUG_LOGGING)
  {
    // Acquisition debug information
    if (acq_debug == NULL)
      acq_debug = fopen("acq_debug_log.dat","w");
    if (acq_debug2 == NULL)
      acq_debug2 = fopen("acq_debug2_log.dat","w");

    // STG testing
    complete_delay_search(ch->prn_num,ch->track.carrier_freq);

    // store fft output for Doppler we think signal is at
    for (s = 0; s < system_vars.acq_buf_len; s++)
    {
    	double dd;
    	dd = inverse_fft[s].r * inverse_fft[s].r + 
           inverse_fft[s].i * inverse_fft[s].i;
      fprintf(acq_debug, "%.5f ",dd);
    }
    fprintf(acq_debug, "\n");
  } // end acquisition debug

  return retval;
}

void complete_delay_search(unsigned int prn, double freq)
{
	char nco_sin, nco_cos, *code;
	int code_idx_prompt;
	unsigned s;
	double fa_energy;
	int chips_per_code;
 
    chips_per_code = CHIPS_PER_CODE;
    code = CODE_TABLE[prn-1];

  unsigned i;
  double code_step = 0.1;
  unsigned loops = (unsigned) (1023.0/code_step);

  double code_inc = (CODE_FREQ) / system_vars.sampling_freq;
  double car_phase_inc = 2 * M_PI * (freq) / system_vars.sampling_freq;
  double code_prompt = 0;
  //Nick: My block of additions
  //double tmp_ip, tmp_qp;
  double inner_code_prompt = 0;//Let us try and use a different code_prompt counter for the inner loop.
  float temp_period;//Nick: Integrate based on milliseconds
  unsigned int integration_period;
  temp_period = ((float)system_vars.acq_buf_len / (float)ACQ_MS) * (float)system_vars.ms_integration;
  integration_period=(unsigned)temp_period;
  //fastgps_printf("Carrier phase increment %lf\n", car_phase_inc);//Nick:
  fastgps_printf("Integration is over %u and acquisition buffer length is %u\n",integration_period, system_vars.acq_buf_len);
  if (integration_period>=system_vars.acq_buf_len)
    integration_period=system_vars.acq_buf_len-1;
    //Nick: End of my block of additions

  for (i = 0; i < loops; i++)
  {
    code_prompt += code_step;
    double car_phase = 0, ip = 0, qp = 0;

    inner_code_prompt = code_prompt;

    for (s = 0; s < 16368; s++)

    {
      char sample = acq_buf[s];

      if (inner_code_prompt > chips_per_code + 1)
        inner_code_prompt -= chips_per_code;

      nco_sin = sample * GPS_SIN(car_phase);
      nco_cos = sample * GPS_COS(car_phase);

      code_idx_prompt = (int)(inner_code_prompt);

      car_phase += car_phase_inc;
      inner_code_prompt += code_inc;


      ip += code[code_idx_prompt] * nco_sin;
      qp += code[code_idx_prompt] * nco_cos;


      if (car_phase > M_PI)
        car_phase -= 2 * M_PI;
      if (inner_code_prompt > chips_per_code + 1)
        inner_code_prompt -= chips_per_code;
    }
    fa_energy = ip*ip + qp*qp;
    fprintf(acq_debug2, "%.5f ",fa_energy);
  }  
  fprintf(acq_debug2, "\n");
}

// **********************************************************************
//  Perform fine Doppler search
// **********************************************************************

void fine_acquisition(struct channel *ch)
{
	char nco_sin, nco_cos, *code;
	int code_idx_prompt;
	unsigned s;
	double fa_max_energy = 0, fa_energy;
	int chips_per_code;

    chips_per_code = CHIPS_PER_CODE;
    code = CODE_TABLE[ch->prn_num-1];

    // Loop through fine Doppler bins at signal code delay (max_shift)
	for (ch->acq.fine_doppler_idx = 0; 
       ch->acq.fine_doppler_idx < NUM_FINE_DOPPLERS; 
       ch->acq.fine_doppler_idx++)
	{
		ch->doppler = dopplers[ch->acq.best_doppler] + 
                  fine_dopplers[ch->acq.fine_doppler_idx];
		ch->code_inc = (CODE_FREQ + ch->doppler * CARRIER_AID_SF) / 
                   system_vars.sampling_freq;
		ch->car_phase_inc = 2 * M_PI * (system_vars.IF + ch->doppler) / 
                        system_vars.sampling_freq;
		ch->code_prompt = chips_per_code - max_shift + 1;
		ch->ip = ch->qp = 0;

        // Perform correlation at this code and freq       
		for (s = 0; s < system_vars.acq_buf_len; s++)
		{
			char sample = acq_buf[s];
			nco_sin = sample * GPS_SIN(ch->car_phase);
			nco_cos = sample * GPS_COS(ch->car_phase);
			ch->car_phase += ch->car_phase_inc;
			code_idx_prompt = (int)(ch->code_prompt);
			ch->code_prompt += ch->code_inc;
			ch->ip += code[code_idx_prompt] * nco_cos;
			ch->qp -= code[code_idx_prompt] * nco_sin;
			if (ch->car_phase > M_PI)
				ch->car_phase -= 2 * M_PI;
			if (ch->code_prompt > chips_per_code + 1)
				ch->code_prompt -= chips_per_code;
		}

        // calculate max energy
		fa_energy = ch->ip*ch->ip + ch->qp*ch->qp;

        // keep the best value
		if (fa_energy > fa_max_energy)
		{
			fa_max_energy = fa_energy;
			ch->acq.best_fine_doppler = ch->acq.fine_doppler_idx;
		}

	}

    // store best value for tracking loops
	ch->doppler = dopplers[ch->acq.best_doppler] + 
                fine_dopplers[ch->acq.best_fine_doppler];
}

// **********************************************************************
//  KissFFT initialization routine
// **********************************************************************

void init_fft_acq()
{
	// sample the C/A code for all satellites
	uint8_t sv_num, ch_idx;
	gps_real_t code_time, code_time_inc;
	unsigned s;
  kiss_fft_scalar *sampled_code;
  kiss_fftr_cfg forward_fft_cfg;
  //kiss_fft_cpx *fft_buf;

	fastgps_printf("precalculating C/A code FFT tables...");
	sample_idx = 0;
	code_time_inc = 1.0 / system_vars.sampling_freq;
  unsigned acq_fft_len = kiss_fft_next_fast_size(system_vars.acq_buf_len);
	sampled_code = (kiss_fft_scalar *)malloc(sizeof(kiss_fft_cpx) * acq_fft_len);
  memset(sampled_code, 0, sizeof(kiss_fft_cpx) * acq_fft_len);
  //system_vars.acq_buf_len = 654;
  fastgps_printf("acq buf len = %d\n", acq_fft_len);
  forward_fft_cfg = kiss_fftr_alloc(acq_fft_len, 0, NULL, NULL);
	acq_buf_write_pos = 0;

	for (sv_num = 0; sv_num < MAX_SATELLITES; sv_num++)
	{
		code_fft[sv_num] = (kiss_fft_cpx *)malloc(sizeof(kiss_fft_cpx) * 
                                              acq_fft_len);
        memset(code_fft[sv_num], 0, sizeof(kiss_fft_cpx) * acq_fft_len);
		code_time = 0;
		for (s = 0; s < system_vars.acq_buf_len / ACQ_MS; s++)
		{
			uint8_t ms_counter;
			code_time += code_time_inc;
			for (ms_counter = 0; ms_counter < ACQ_MS; ms_counter++)
				sampled_code[s + ms_counter * system_vars.acq_buf_len / ACQ_MS] = CODE_TABLE[sv_num][(int)floor(CHIPS_PER_CODE * 1000 * code_time) + 1];

		}
    kiss_fftr(forward_fft_cfg, sampled_code, code_fft[sv_num]);

		for (s = 0; s < system_vars.acq_buf_len; s++)
			code_fft[sv_num][s].i *= -1;
	}
	sample_buf = (kiss_fft_cpx *)malloc(sizeof(kiss_fft_cpx) * acq_fft_len);
	sample_fft = (kiss_fft_cpx *)malloc(sizeof(kiss_fft_cpx) * acq_fft_len);
	inverse_fft = (kiss_fft_cpx *)malloc(sizeof(kiss_fft_cpx) * acq_fft_len);
	mult_result = (kiss_fft_cpx *)malloc(sizeof(kiss_fft_cpx) * acq_fft_len);
  for (unsigned i = 0; i < acq_fft_len; i++)
  {
    sample_buf[i].r = sample_buf[i].i = 0;
    sample_fft[i].r = sample_fft[i].i = 0;
    inverse_fft[i].r = inverse_fft[i].i = 0;
    mult_result[i].r = mult_result[i].i = 0;
  }
  sample_fft_cfg = kiss_fft_alloc(acq_fft_len, 0, NULL, NULL);
  inverse_fft_cfg = kiss_fft_alloc(acq_fft_len, 1, NULL, NULL);
	max_shift_energy = (double *)malloc(sizeof(double) * acq_fft_len);
  memset(max_shift_energy, 0, sizeof(double) * acq_fft_len);
	avg_energy = 0;
	max_energy = 0;
	for (ch_idx = 0; ch_idx < MAX_CHANNELS; ch_idx++)
		c[ch_idx].acq.failure_count = 0;
  kiss_fft_free(forward_fft_cfg);
  free(sampled_code);
	fastgps_printf("done\n");
}

// **********************************************************************
//  KissFFT clean up
// **********************************************************************

void shutdown_fft_acq() {

  for (uint8_t sv = 0; sv < MAX_SATELLITES; sv++)
    free(code_fft[sv]);
  kiss_fft_free(sample_fft_cfg);
  kiss_fft_free(inverse_fft_cfg);
  free(sample_buf);
  free(sample_fft);
  free(mult_result);
  free(max_shift_energy);
  free(inverse_fft);
  kiss_fft_cleanup();

}

// **********************************************************************
//  Read in existing acquisition file and assign values to tracking variables
// **********************************************************************

int read_acquisiton_file()
{
  double testd = 0;
  int testi=0;
  char tempc=0;
  FILE *infile;
  unsigned tempchan = 0;

  /* Open configuration file */
  infile = fopen("fastgps_acquisition_log.dat","r");
  if (!infile)
  {
    printf("woah! couldn't open the acquistion log. please don't select "
           "in the config file.\n");
    exit(1);
  }
  rewind(infile);  // make sure we are at the beginning

  /* Read in info from file */
  	while (!feof(infile))  /* until end of file */
  	{
  		VERIFY_IO_FEOF(fread(&tempc,1,1,infile), 1, infile);
  		if(tempc == '/')
      {
  	  	VERIFY_IO(fread(&tempc,1,1,infile), 1);
        if(tempc == 'A')
        {
          /* Read SV info entry */
          VERIFY_IO(fscanf(infile," %lf",&testd), 1);   // process time
          VERIFY_IO(fscanf(infile," %d",&testi), 1);
          system_vars.acq_file_start_count = (unsigned) testi;
          VERIFY_IO(fscanf(infile," %d",&testi), 1);  // sv
          VERIFY_IO(fscanf(infile," %d",&testi), 1);  // chan
          tempchan = (unsigned) testi;
          init_correlator_channel(tempchan);
          VERIFY_IO(fscanf(infile," %d",&testi), 1);  // sv actual one assigned
          c[tempchan].prn_num = testi;
          VERIFY_IO(fscanf(infile," %lf",&testd), 1);  // max energy
          VERIFY_IO(fscanf(infile," %lf",&testd), 1);   // acq ratio
          VERIFY_IO(fscanf(infile," %lf",&testd), 1);   // Doppler
          // the whole enchilada (for now)
          VERIFY_IO(fscanf(infile," %lf",&testd), 1);
          c[tempchan].true_car_phase_inc = testd;
          VERIFY_IO(fscanf(infile," %lf",&testd), 1);
          c[tempchan].true_code_inc = testd;
          VERIFY_IO(fscanf(infile," %lf",&testd), 1);
          c[tempchan].track.carrier_freq = testd;
          c[tempchan].track.carrier_freq_acq = testd;
          VERIFY_IO(fscanf(infile," %lf",&testd), 1);
          c[tempchan].track.code_freq = testd;
          VERIFY_IO(fscanf(infile," %lf",&testd), 1);
          c[tempchan].doppler = testd;
          VERIFY_IO(fscanf(infile," %lf",&testd), 1);
          c[tempchan].code_prompt = testd;
          VERIFY_IO(fscanf(infile," %lf",&testd), 1);
          c[tempchan].acq.acq_finish_time = (unsigned)testd;
          c[tempchan].state = CH_STATE_POSTACQ_SPIN;
          fastgps_printf("Allocating PRN %d to channel %d\n",
                         c[tempchan].prn_num, tempchan);
          system_vars.num_channels++;
        }  // if 'A'
      }  // if '\'
    }  // end while
  return system_vars.num_channels;
}

void update_acq_log()
{
  if(system_vars.acq_log_flag >= NORMAL_LOGGING)
  {
    // one log used for re-acquisiiton, the other for plotting because
    // Octave/Matlab and Python don't like to load characters into arrays nicer
    // ways to do this are invited.
    if(system_vars.acq_log == NULL)
      system_vars.acq_log = fopen("cfastgps_acquisition_log.dat","w");
    if(system_vars.acq_log2 == NULL)
      system_vars.acq_log2 = fopen("cfastgps_acquisition_log_plot.dat","w");

    if((system_vars.acq_log != NULL) && (system_vars.acq_log2 != NULL))
    {
      for (int sv = 1; sv <= MAX_SATELLITES; sv++)
      {
        uint64_t tempul64 = (1 << sv);
        if(tempul64 & system_vars.sats_found)
        {
          unsigned tempchan = system_vars.prn_to_channel_map[sv];
          // write acq info for this channel, time and loop reference, start
          // with /A to enable use of this info during future runs with the
          // same data
          fprintf(system_vars.acq_log, "/A %.5f %lu ",
                  system_vars.process_time, system_vars.loop_count);
          fprintf(system_vars.acq_log2, "0 %.5f %lu ",
                  system_vars.process_time, system_vars.loop_count);
          char msg[1024];
          sprintf(msg, "%d %d %d %f %f %f ", sv, tempchan,
                  c[tempchan].prn_num, 0.0, c[tempchan].acq.ratio, 0.0);
          fputs(msg,system_vars.acq_log);
          fputs(msg,system_vars.acq_log2);

          // needed when performing file acquisition
          sprintf(msg, "%12.11f %12.11f %lf %12.5f %12.5f %12.9f %d \n",
                  c[tempchan].true_car_phase_inc,
                  c[tempchan].true_code_inc,
                  c[tempchan].track.carrier_freq,
                  c[tempchan].track.code_freq,
                  c[tempchan].doppler,
                  c[tempchan].code_prompt,
                  c[tempchan].acq.acq_finish_time);
          fputs(msg,system_vars.acq_log);
          fputs(msg,system_vars.acq_log2);
        }
        else
        {
          // write zeros, no sat on this channel, /N indicates satellite was
          // not acquired
          fprintf(system_vars.acq_log, "/N %.5f %lu ",
                  system_vars.process_time, system_vars.loop_count);
          fprintf(system_vars.acq_log2, "0 %.5f %lu ",
                  system_vars.process_time, system_vars.loop_count);
          sprintf(msg, "%d %d %d %f %f %f %f %f %f %f %f %f %d \n",
                  sv, 0, 0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0);
          fputs(msg,system_vars.acq_log);
          fputs(msg,system_vars.acq_log2);
        }
      }  // end of sv loop
    }
  }
}
