//    datetime.cpp - This is a modified version of the Date/Time class
//             originally created by Charles Schwarz and Gordon Adams
//             -- Steve Hilla, 1 May 2000.
//
//0005.01, SAH, Change code to use mjd, fmjd as the private data. The
//              difference between two DateTime objects will be in "days".
//0012.27, SAH, Change LeapMonths[] and NormalMonths[] to hold 13 elements.
//0103.13, SAH, modify the >, >=, <, <= manipulator member functions.
//0104.11, SAH, Add an output operator function ( << ).
//0105.08, SAH, add normalize function to prevent HMS output where sec = 60.0
//0606.19, SAH, add code to DateTime::operator + ( const double days ), to 
//              handle cases where the input value days is negative.
// Translation in C and removing of useless features by Marco Dalla Mutta (2018)

#include "datetime.h"

#if !defined(MATH_)
#include <math.h>
#define MATH_


#include <stdbool.h>
#include <stdio.h>

const long     JAN61980 = 44244;
const long     JAN11901 = 15385;
const double   SECPERDAY = 86400.0;

const long LeapMonths[13]   = { 0,  31,  60,  91, 121, 152, 182,
                                  213, 244, 274, 305, 335, 366 };

const long NormalMonths[13] = { 0,  31,  59,  90, 120, 151, 181,
                                  212, 243, 273, 304, 334, 365 };

DateTime _DateTime(){

   DateTime datetime;
   datetime.mjd = 0;
   datetime.fractionOfDay = 0.0;

   return datetime;

}

DateTime GPSDateTime(GPSTime gpstime) {

   DateTime datetime = _DateTime();
   datetime.mjd = gpstime.GPSWeek * 7 + (long) (gpstime.secsOfWeek / SECPERDAY) + JAN61980;
   datetime.fractionOfDay = fmod(gpstime.secsOfWeek, SECPERDAY) / SECPERDAY;
   return datetime;

}

DateTime yearMDDateTime2(long year, long month, long day,
                    long hour, long min, double sec ){

   DateTime datetime;
   long doy;

   if( year%4 == 0 )   // good until the year 2100
    doy = LeapMonths[month - 1] + day;
   else
    doy = NormalMonths[month - 1] + day;

   datetime.mjd = ((year - 1901)/4)*1461 + ((year -
           1901)%4)*365 + doy - 1 + JAN11901;

   datetime.fractionOfDay = ((sec/60.0 + min)/60.0 + hour)/24.0;
   return datetime;

}



void SetYMDHMS2(DateTime *datetime, long year, long month, long monthday,
                          long hour, long min, double sec){

    *datetime = yearMDDateTime2(year, month, monthday, hour, min, sec);

}

DateTime timesum(DateTime datetime, const double days){

   DateTime DT;
   DT.mjd           = datetime.mjd;
   DT.fractionOfDay = datetime.fractionOfDay + days;

   if( DT.fractionOfDay > 1.0 ){

       double wholeDays = floor( DT.fractionOfDay );
       DT.mjd = DT.mjd + (long) wholeDays;
       DT.fractionOfDay = DT.fractionOfDay - wholeDays;

   }

   if( DT.fractionOfDay < 0.0 ){

       double wholeDays = ceil( DT.fractionOfDay ); 
       DT.mjd = DT.mjd - (long) wholeDays - 1;
       DT.fractionOfDay = 1.0 + (DT.fractionOfDay - wholeDays);
   }

   return DT;

}

double timediff(DateTime DT1, DateTime DT2){

   return (  ( ((double)DT1.mjd - (double)DT2.mjd) + DT1.fractionOfDay ) -
               DT2.fractionOfDay );
}

#endif
