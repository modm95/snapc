//Added by Marco Dalla Mutta (2018)
//Puts all useful functions that have no specific category into one file

#include "fastgps.h"
#include <errno.h>
#include <stdarg.h>
#include <string.h>

s_system_vars system_vars;

unsigned int strike_off_PRN; //Nick for removing a PRN from a snapshot position solution matrix

void fastgps_printf(const char *format, ...) {

    va_list args;
    va_start(args, format);
    vprintf(format, args);
    va_end(args);

}

#ifdef WIN32

char *strndup(const char *s, size_t n) {
    char *p = memchr(s, '\0', n);
    if (p != NULL)
        n = p - s;
    p = malloc(n + 1);
    if (p != NULL) {
        memcpy(p, s, n);
        p[n] = '\0';
    }
    return p;
}

int getline(char **lineptr, size_t *n, FILE *stream){

    static char line[256];
    char *ptr;
    unsigned int len;

    if (lineptr == NULL || n == NULL)
    {
        errno = EINVAL;
        return -1;
    }

    if (ferror (stream))
        return -1;

    if (feof(stream))
        return -1;

    fgets(line,256,stream);

    ptr = strchr(line,'\n');
    if (ptr)
        *ptr = '\0';

    len = strlen(line);

    if ((len+1) < 256)
    {
        ptr = realloc(*lineptr, 256);
        if (ptr == NULL)
            return(-1);
        *lineptr = ptr;
        *n = 256;
    }

    strcpy(*lineptr,line);
    return(len);

}

#endif


char* substring(const char* str, size_t begin, size_t len) {

    if (str == 0 || strlen(str) == 0 || strlen(str) < begin || strlen(str) < (begin+len))
        return 0;

    return strndup(str + begin, len);

}


int read_config_file()
{
  double testd = 0;
  int testi=0;
  char tempc=0;
  int i,num_sats;
  unsigned ch_idx;

  memset(system_vars.infilename,0,200);

  /* Open configuration file */
  system_vars.config_file = fopen("snapc_config.txt","r");
  if (!system_vars.config_file)
  {
    fastgps_printf("Couldn't open configuration file 'snapc_config.txt'.\n");
//    exit(1);
    return PROBLEM;
  }
  rewind(system_vars.config_file);  // make sure we are at the beginning

 /* Read in info from file */
  if(!system_vars.config_file)
  {
    fastgps_printf("couldn't open config file.\n");
    return PROBLEM;
  }
  else
  {

  	while(!feof(system_vars.config_file))  /* until end of file */
    {

      VERIFY_IO_FEOF(fread(&tempc,1,1,system_vars.config_file), 1, system_vars.config_file);

      if(tempc == '^')
      {
        VERIFY_IO(fread(&tempc,1,1,system_vars.config_file), 1);
        if(tempc == 'A')
        {
          /* Aiding information */
          VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
          system_vars.pvt_aiding_flag = testi;
          VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
          system_vars.datafile_week = testi;
          VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
          system_vars.datafile_day = testi;
          /* IGS Data File */
          VERIFY_IO(fscanf(system_vars.config_file, " %200s",
                           system_vars.IGSfilename), 1);

        }  // end if 'A'
        if(tempc == 'D'){ // data file
         VERIFY_IO(fscanf(system_vars.config_file," %200s",
                           system_vars.infilename), 1);

        }
        if(tempc == 'E') // file ephemeris flag
        {
          VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
          system_vars.file_ephemeris = system_vars.file_ephemeris_present = (unsigned)testi;

        }
        //Nick: Channels to strike off the position solution
        if(tempc == 'N') // Channel flag
        {
          VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
          system_vars.channel_cancel[0] = testi;
          VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
          system_vars.channel_cancel[1] = testi;
          VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
          system_vars.channel_cancel[2] = testi;
          VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
          system_vars.channel_cancel[3] = testi;
          VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
          system_vars.channel_cancel[4] = testi;
          VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
          system_vars.channel_cancel[5] = testi;
          VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
          system_vars.channel_cancel[6] = testi;
          VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
          system_vars.channel_cancel[7] = testi;
          VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
          system_vars.channel_cancel[8] = testi;
          VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
          system_vars.channel_cancel[9] = testi;
          VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
          system_vars.channel_cancel[10] = testi;
          VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
          system_vars.channel_cancel[11] = testi;

        }//End if 'N' Added by Nick
        //Nick: Set the number of ms over which to integrate for visual peak detection during acquisition
        if(tempc == 'P')
        {
            VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
            system_vars.ms_integration = testi;
        }
        //Nick: PRN to strike off the position solution in snapshot positioning
        if(tempc == 'Q') // PRN strike off flag
        {
          VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
          strike_off_PRN = testi;
        }
        if(tempc == 'F')
        {
          VERIFY_IO(fscanf(system_vars.config_file," %lf",&testd), 1);
          system_vars.sampling_freq = testd;

          //Nick: Ouput the value of samplning frequency
          fastgps_printf("The sampling frequency is %7.4lf\n", testd);

          // other variables dependant on sampling_freq
          system_vars.sample_period = (1.0 / system_vars.sampling_freq);
          system_vars.acq_buf_len = (unsigned)(ACQ_MS * system_vars.sampling_freq /1000);
          VERIFY_IO(fscanf(system_vars.config_file," %lf",&testd), 1);
          system_vars.IF = testd;

        }  // end if 'F'
        if(tempc == 'I')
        {
          VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
          system_vars.testi = testi;
          VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
          system_vars.snap_shot_flag = testi;
          VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
          system_vars.doppler_pos_flag = testi;

          VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
          system_vars.estimate_gpsweek = testi;
          VERIFY_IO(fscanf(system_vars.config_file," %lf",&testd), 1);
          system_vars.estimate_gpssecs = testd;
          system_vars.doppler_positioning_run_status = 0;//Nick
          system_vars.time_free_positioning_run_status = 0;//Nick
          system_vars.previous_process_time = 0;//Nick
          system_vars.temp_estimate_gpssecs = testd;//Nick

          VERIFY_IO(fscanf(system_vars.config_file," %lf",&testd), 1);
          system_vars.estimate_wgs84_pos[0] = testd;
          VERIFY_IO(fscanf(system_vars.config_file," %lf",&testd), 1);
          system_vars.estimate_wgs84_pos[1] = testd;
          VERIFY_IO(fscanf(system_vars.config_file," %lf",&testd), 1);
          system_vars.estimate_wgs84_pos[2] = testd;

          VERIFY_IO(fscanf(system_vars.config_file," %lf",&testd), 1);
          system_vars.PVT_INTERVAL = testd;
          VERIFY_IO(fscanf(system_vars.config_file," %lf",&testd), 1);
          system_vars.LOOP_TIME = testd;

          system_vars.time_status = 0;
          system_vars.position_status = 0;

          if((system_vars.estimate_gpsweek != 0) &&
             (system_vars.estimate_gpssecs != 0)){
                system_vars.time_status = HAVE_TIME_FILE__ESTIMATE;
             }


          if((system_vars.estimate_wgs84_pos[0] != 0) &&
             (system_vars.estimate_wgs84_pos[1] != 0) &&
             (system_vars.estimate_wgs84_pos[2] != 0)){
                system_vars.position_status = HAVE_WGS84_FILE_ESTIMATE;
             }

        }  // end if 'I'
        if(tempc == 'L') // logging flags
        {
          VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
          system_vars.acq_log_flag = testi;
          VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
          system_vars.tracking_log_flag = testi;
          VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
          system_vars.nav_log_flag = testi;

        }  // end if 'L'
        if(tempc == 'S') // satellite info
        {
          VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
          num_sats = testi;
          if(num_sats == 99) // search all satellites
          {
            for (i = 0; i < MAX_SATELLITES; i++)
              system_vars.prn_search_list[i] = i+1;
            system_vars.num_search_svs = MAX_SATELLITES;
            system_vars.num_sats_to_track = MAX_SATELLITES_TO_TRACK;
          }
          else if(num_sats == 100)
          {
            // use file for acquisition
            system_vars.file_acquisition = ON;
          }
          else if(num_sats < MAX_SATELLITES)
          {
            // read in specific satellties to search for
            for (i = 0; i < num_sats; i++)
            {
              VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
              system_vars.prn_search_list[i] = testi;
            }
            system_vars.num_search_svs = num_sats;
            system_vars.num_sats_to_track = num_sats;
          }

        }  // end if 'S'
        if(tempc == 'T') // time to process, in seconds
        {
          VERIFY_IO(fscanf(system_vars.config_file," %lf",&testd), 1);
          system_vars.run_time = testd;

        }  // end if 'T'
        if(tempc == 'G')
        {
          int interval1,interval2,interval3;
          double Kco_temp, Kco2_temp, Kca2_fll1_temp, Kca2_fll2_temp;
          double Kca2_pll_temp, Kca3_pll_temp;
          /* Tracking loop intervals and gains */
          VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
          interval1 = testi;
          VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
          interval2 = testi;
          VERIFY_IO(fscanf(system_vars.config_file," %d",&testi), 1);
          interval3 = testi;
          VERIFY_IO(fscanf(system_vars.config_file," %lf",&testd), 1);
          Kco_temp = testd;
          VERIFY_IO(fscanf(system_vars.config_file," %lf",&testd), 1);
          Kco2_temp = testd;
          VERIFY_IO(fscanf(system_vars.config_file," %lf",&testd), 1);
          Kca2_fll1_temp = testd;
          VERIFY_IO(fscanf(system_vars.config_file," %lf",&testd), 1);
          Kca2_fll2_temp = testd;
          VERIFY_IO(fscanf(system_vars.config_file," %lf",&testd), 1);
          Kca2_pll_temp = testd;
          VERIFY_IO(fscanf(system_vars.config_file," %lf",&testd), 1);
          Kca3_pll_temp = testd;
          for (ch_idx = 0; ch_idx < MAX_CHANNELS; ch_idx++)
          {
            struct channel *ch = &c[ch_idx];
            ch->track.Kco = Kco_temp;
            ch->track.Kco2 = Kco2_temp;
            ch->track.Kca2_FLL1 = Kca2_fll1_temp;
            ch->track.Kca2_FLL2 = Kca2_fll2_temp;
            ch->track.Kca2_PLL = Kca2_pll_temp;
            ch->track.Kca3_PLL = Kca3_pll_temp;
            ch->track.fll_switch_time = interval1;
            ch->track.pll_switch_time = interval2;
            ch->track.pullin_time = interval3;
          }

        }  // end if 'G'
        if(tempc == 'W')
        {			
		  VERIFY_IO(fread(&tempc,1,1,system_vars.config_file), 1);


          /* WAAS corrections file */

              VERIFY_IO(fscanf(system_vars.config_file, "%200s",
                               system_vars.WAASfilename), 1);
              system_vars.waas_flag = YES;


         }
        }  // end if 'W'
      }  // if '\'
    }  // end while
    return 0;
  }  // end else




