#include "fastgps.h"
#include <limits.h>
#include <pthread.h>
#include <time.h>

extern FILE *acq_debug;
extern FILE *acq_debug2;
extern FILE *nav_debug;
extern gps_real_t dopplers[];
extern gps_real_t fine_dopplers[];

pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t lock2 = PTHREAD_MUTEX_INITIALIZER;

void track(char* data_buf, size_t bytes_read);
void* acquire_thread(void* arg);

int main(void) {
	
  FILE *data_file;
  char *data_buf;
  unsigned i, j;
  unsigned retval,retval2;

  fastgps_printf("\r\nWelcome to Snapc\r\n");
  fastgps_printf("\r\nThis program uses cfastgnss, a deeply-modified and translated in C version of fastgps\r\n");

  // initialize misc tracking and nav variables
  memset(&system_vars,0,sizeof(struct s_system_vars));
  system_vars.Rx_State = ACQ;   // initial state is acquire
  tracking_init();
  nav_init();

  // read the config file
  system_vars.PVT_INTERVAL = XPVT_INTERVAL;  // just in case its not defined in file
  retval = (unsigned) read_config_file();

  if (retval == PROBLEM) {

    fastgps_printf("Couldn't read config file.\nCheck that "
                   "snapc_config.txt is in executable directory.\n");
    return -1;

  }

  // open the data file and allocate temporary data buffer
  data_file = fopen(system_vars.infilename,"rb");
  data_buf = (char *) malloc(DATA_BUF_SIZE);

  // Set the IGS file name from GPS day and week
  if(system_vars.pvt_aiding_flag) {

    fastgps_printf("Using sp3 file for navigation information.\n");
    system_vars.nav_GPS_week = system_vars.datafile_week;

  }

  else {

      // TODO: determine the week based on the data file
	  // for now i'll just require aiding file
	  fastgps_printf("Aiding sp3 file required\n");
	  return -1;
	  
  }

  if (!data_file) {

    fastgps_printf("Couldn't open data file.\nCheck that path to data "
                   "file in snapc_config.txt is correct.\n");
    return -1;

  }

  // Initialize Doppler bins used during coarse and fine acquisition
  // be warned that the decimal point after the 2's are really necessary!
  // otherwise the compiler generates an unsigned multiply and things get
  // really ugly...
  for (i = 0; i < NUM_COARSE_DOPPLERS; i++)
    dopplers[i] = i * 2.0 * DOPPLER_RADIUS / (NUM_COARSE_DOPPLERS - 1) -
                  DOPPLER_RADIUS;
  for (i = 0; i < NUM_FINE_DOPPLERS; i++)
    fine_dopplers[i] = i * 2. * FINE_DOPPLER_RADIUS / (NUM_FINE_DOPPLERS - 1) -
                       FINE_DOPPLER_RADIUS;

  // Initialize tracking channels
  for (i = 0; i < MAX_CHANNELS; i++)
    init_correlator_channel(i);

  // Misc Initialization
  if(system_vars.file_acquisition == OFF)
        init_fft_acq(); // if we are not using an acquisition file

  system_vars.search_channel = 0;
  system_vars.ms_update_flag = OFF;

  double loop_time = DATA_BUF_SIZE/system_vars.sampling_freq;
  double watch_interval = 0.5;     // approximate
  unsigned loops_per_watch = (unsigned)(watch_interval / loop_time);
  system_vars.next_pvt_time = 0.0;

  if(system_vars.acq_log_flag == DEBUG_LOGGING)
    fastgps_printf("Generating Acquisition Debug Information.\n");
  if(system_vars.nav_log_flag == DEBUG_LOGGING)
    fastgps_printf("Generating Navigation Debug Information.\n");

  /* ********************* */
  /* Main Loop Starts Here */
  /* ********************* */

  fastgps_printf("Starting Snapshot Positioning\n");
  int waited = 0;
  double process_time;
  clock_t start = {0}, end;
  double cpu_time_used = 0.0;
  if(system_vars.PVT_INTERVAL == 0)
	  system_vars.PVT_INTERVAL = UINT_MAX;  //if PVT_INTERVAL is 0, maximize the running time (ideally we would like it to be infinite)
  
  while (!feof(data_file) && (cpu_time_used + system_vars.PVT_INTERVAL * waited < system_vars.run_time)){

    // Read Data from file
    size_t bytes_read = fread(data_buf, 1, DATA_BUF_SIZE, data_file);
    system_vars.process_time = system_vars.loop_count * loop_time;
	process_time = system_vars.process_time + system_vars.PVT_INTERVAL * waited;		   
    system_vars.loop_count++;
    // Print messages every so often
	
    if (system_vars.loop_count % loops_per_watch == 0){  //messages = time info
		
      fastgps_printf("processing time = %.3f\n", process_time);
	  
	  if(waited){

		fastgps_printf("total time = %.3f\n", cpu_time_used + system_vars.PVT_INTERVAL * waited);
		fastgps_printf("total time in pause = %.3f (%d%% of total time)\n", system_vars.PVT_INTERVAL * waited,
		        (int)((system_vars.PVT_INTERVAL * waited)*100/(cpu_time_used + system_vars.PVT_INTERVAL * waited)));

	  }

	}

    // if receiver time has been set, update over this data interval
    if(system_vars.recv_time_valid)
      system_vars.recv_time += (double)bytes_read / system_vars.sampling_freq;

    /* ********************* */
    /*    Acquisition        */
    /* ********************* */

    if(system_vars.Rx_State == ACQ) {

        if(system_vars.file_ephemeris_present == OK)
            system_vars.file_ephemeris = ON;

      // add more data to the acquisition buffer
      for (unsigned sidx = 0; sidx < bytes_read; sidx++) {

        acq_buf[acq_buf_write_pos++] = data_buf[sidx];
        if (acq_buf_write_pos >= system_vars.acq_buf_len) {

          for (unsigned ch_idx = 0; ch_idx < MAX_CHANNELS; ch_idx++)
            c[ch_idx].acq.acq_finish_time = sidx;

          break;  // we have enough data for acquisition

        }

      }

      if(system_vars.file_acquisition == ON) {

        // read acquisition data in from file if the acq data buffer is full,
        // perform search, start at the same point, even with file acquisition
        if (acq_buf_write_pos >= system_vars.acq_buf_len) {

          read_acquisiton_file();
          if(system_vars.num_channels > 0) {

            system_vars.Rx_State = TRACKING;  // we found at least 1 satellite
            fastgps_printf("Satellites acquired using input file.\n"
                           "Starting tracking.\n");

          }

          else {

            system_vars.Rx_State = LOST;     // we have found nothing, exit
            fastgps_printf("No satellites acquired using input file.\n");

          }

        }

      }

      else {

        // search for satellites specified in the config file
        // if the acq data buffer is full, perform search
        if (acq_buf_write_pos >= system_vars.acq_buf_len){

          pthread_t threads[system_vars.num_search_svs];
          int ints[system_vars.num_search_svs];

          for (j = 0; j < system_vars.num_search_svs; j++){

            ints[j] = j;
            pthread_create(&threads[j], NULL, acquire_thread, &ints[j]); //trying to use multithreading for optimization

            pthread_mutex_lock(&lock);
            if(system_vars.num_channels >= MAX_CHANNELS || j == system_vars.num_search_svs - 1){
				
				pthread_mutex_unlock(&lock);
                break;  // we have sats for all the channels; break out of search
				
		    }
			
		    if(system_vars.num_channels >= MINIMUM_PVT_SATELLITES){  //with minimum satellites we can already try to do tracking and positioning
							
				fastgps_printf("Trying Early Snapshot Position Calculation\n");
			
				track(data_buf, bytes_read);

				retval = SnapPosition();
				
				if (retval == SUCCESS){
					
					fastgps_printf("Position obtained!\n");
                    update_nav_log(); // update navigation logs
					
				}
				
			}			
		 
			pthread_mutex_unlock(&lock);
		  
			bytes_read = fread(data_buf, 1, DATA_BUF_SIZE, data_file); //after one PRN search, update data buffer
		      
			if(system_vars.recv_time_valid)
				system_vars.recv_time += (double)bytes_read / system_vars.sampling_freq;
		 			
			
			pthread_join(threads[j], NULL);

          }  // end of search_svs loop

          for (j = 0; j < system_vars.num_search_svs; j++)
              pthread_join(threads[j], NULL); //here we ensure that all threads are terminated (this is useful when not all threads are created,
                                              //so when the break is executed)
          // exit acquisition
          if(system_vars.num_channels >= MINIMUM_PVT_SATELLITES){
			  
            system_vars.Rx_State = TRACKING;  
            fastgps_printf("Starting Tracking.\n");

          }
		  
          else
            system_vars.Rx_State = ACQ;     // we don't have the minimum number of satellites, retry
          // log results of acquisition search
          update_acq_log();

        }// end if  acq data buffer is full

      }  // end else file_acquisition

    }     // end of acquisition

    /* ********************* */
    /*    Tracking           */
    /* ********************* */

    if(system_vars.Rx_State == TRACKING){

        if(system_vars.file_ephemeris_present == OK && system_vars.file_ephemeris == ON){

            //i read the eph file when i have full info on channels
            read_ephemeris_file();
            system_vars.file_ephemeris = OFF; // only read file once

        }

      track(data_buf, bytes_read);

      /* ********************* */
      /*    Navigation         */
      /* ********************* */

      /* *********************************** */
      /*    Snapshot Positioning Attempt    */
      /* *********************************** */
      retval2 = 0;
      if((system_vars.process_time > 0.5)
          && (system_vars.process_time > system_vars.next_pvt_time)
          && (system_vars.position_status != 0)
          && (system_vars.snap_shot_flag == YES)){
		  
         waited++;
         system_vars.Rx_State = NAV;
         		
         fastgps_printf("Performing Snapshot Position Calculation.\n");

		  if(!(double)start)
		    start = clock(); //initialize time measurements on first positioning

          system_vars.next_pvt_time = system_vars.process_time + system_vars.PVT_INTERVAL;
          retval2 = (unsigned) SnapPosition();

      }

      if(retval2 == SUCCESS){

          fastgps_printf("Position obtained!\n");
          update_nav_log(); // update navigation logs

      }
	  
	  system_vars.Rx_State = TRACKING;

    } // end of Rx_State == TRACKING
	
	end = clock();  //time measurements
	
	if((double)start){
		
	   cpu_time_used += ((double) (end - start)) / CLOCKS_PER_SEC;
	   start = end;
	   
	}
	
  }  // end of while loop

  // clean up and exit
  free(data_buf);
  if(system_vars.file_acquisition == OFF)
    shutdown_fft_acq();

  // close normal log files
  if(system_vars.acq_log != NULL)
    fclose(system_vars.acq_log);

  if(system_vars.acq_log2 != NULL)
    fclose(system_vars.acq_log2);

  if(system_vars.tracking_log != NULL)
    fclose(system_vars.tracking_log);

  if(system_vars.nav_log != NULL){

        fclose(system_vars.nav_log);
        fclose(system_vars.nick_log);//testing purposes
        fclose(system_vars.sat_log);//test store of satellite locations

    }

  if(system_vars.google_log != NULL){

    /* write the footer to Google Earth file, and close */
    sprintf(msg,"</Folder>\n</kml>");
    fputs(msg,system_vars.google_log);
    fclose(system_vars.google_log);

  }

  // close debug log files
  if(acq_debug != NULL)
    fclose(acq_debug);
  if(acq_debug2 != NULL)
    fclose(acq_debug2);
  if(nav_debug != NULL)
    fclose(nav_debug);
  // close data file
  fclose(data_file); 

  fastgps_printf("Done!\n");

  return 0;
  
}

void track(char* data_buf, size_t bytes_read){

    system_vars.num_valid_tow = 0;
    system_vars.num_valid_eph = 0;
    for (unsigned ch_idx = 0; ch_idx < system_vars.num_channels; ch_idx++) {

        c[ch_idx].nav.valid_for_pvt = NO;
        c[ch_idx].nav.pseudorange_valid  = NO;
        if (c[ch_idx].state != CH_STATE_ACQUIRE){

            // if we have allocated an acquired satellite to this channel
            // call correlator and tracking functions
            software_correlator(&c[ch_idx], data_buf + c[ch_idx].acq.acq_finish_time, (unsigned int)bytes_read - c[ch_idx].acq.acq_finish_time);

            if(c[ch_idx].nav.nav_data_state >= HAVE_TOW)
                system_vars.num_valid_tow++;
            if(c[ch_idx].nav.nav_data_state == HAVE_EPH)
                system_vars.num_valid_eph++;

            // only on the first correlation after acquistion do we adjust for
            // acq_finish_time
            c[ch_idx].acq.acq_finish_time = 0;

        }

    }  // end of channel loop

    // update tracking log
    if ((system_vars.tracking_log_flag == NORMAL_LOGGING && system_vars.ms_update_flag >= 20) || (system_vars.tracking_log_flag == DEBUG_LOGGING &&
       system_vars.ms_update_flag >= 1)) {

        update_tracking_log();
        system_vars.ms_update_flag = OFF;

    }
		
}


void* acquire_thread(void* arg){ //unfortunately acquire2() and the entire acquire phase seem to be based on modification of global variables
                                 //this means we can optimize only the time that is needed to switch from one PRN acquisition to another
    int j = *(int *)arg;
    pthread_mutex_lock(&lock);

    unsigned x = system_vars.search_channel;
    unsigned y = c[x].prn_num = (uint8_t)system_vars.prn_search_list[j];
    uint64_t z = system_vars.sats_found;
    fastgps_printf("Searching for PRN %d\n", y);
    int retval = (int)acquire2(x);
    // perform search for sat on single channel
    pthread_mutex_unlock(&lock);

    if(retval == SUCCESS)
    {

        fastgps_printf("Found PRN %d, allocated to channel %d\n", y, x);
        // if satellite found, don't search for it anymore
        z|= ((uint64_t)1 << y);
        // store channel this sv will be tracked on
        pthread_mutex_lock(&lock2);

        system_vars.sats_found = z;
        system_vars.prn_to_channel_map[y] = x;
        // rotate to next channel
        system_vars.search_channel++;
        init_correlator_channel((uint8_t)system_vars.search_channel);
        system_vars.num_channels++;

        pthread_mutex_unlock(&lock2);
        return NULL;

    }

    fastgps_printf("Did not find PRN %d\n", y);
    return NULL;

}