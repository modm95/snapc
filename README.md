Welcome to SnapC, a software-defined receiver written totally in C that performs Snapshot Positioning.
It uses CFastGPS, a translation in C and modification of fastgps C++ library. You can find the original fastgps project at https://sourceforge.net/projects/fastgps/ .
Tested on Windows and on Ubuntu Linux.

## Building

The receiver compiles only if the OS supports POSIX. That means that in Windows you'll have to use some compiler that supports POSIX, such as MinGW-w64 with POSIX threads or Cygwin (but in the latter case the snapc executable will depend on cygwin1.dll library).
Install CMake and be sure to have a C compiler and POSIX libraries, then you can execute

	cmake .
	make

In case of compilation in Windows with MinGW the commands become

	cmake . -G "MinGW Makefiles"
	mingw32-make

This will generate snapc executable and libcfastgps shared library in the bin folder. No further steps are required.

## How it works:

1 Provide data -> a dataset that contains signals taken through a RF front-end and a sp3 file for aiding positioning (surely works with sp3 type a, b and c, should work also with d). An example is included in this repo.
2 Tweak "snapc_config.txt" -> add the paths of the files and more input (more info in the txt)
3 Start snapc.exe -> if everthing is ok, after a startup time, the program starts to perform Snapshot Positioning
4 On success, every positioning is saved in a kml format file, readable from Google Maps/Earth or other third-party tools

## License

This software is distributed under the GPLv2, as the original fastgps project was.
More about this in the LICENSE file.